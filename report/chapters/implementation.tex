\chapter{Implementation \label{ch:3}}

The implementation of the methods seen in this project has been done in
\textbf{Python}, using \verb!numpy!
\cite{2020NumPy-Array} and
\verb!scipy! \cite{2020SciPy-NMeth} for numerical computing along
some specific librairies and scripts for other aspects of the problem. The
project is accessible at this 
\href{https://gitlab.com/soldat321/optimal-design-in-geophysical-imaging}{repository}.

\section{Wavenumbers}

A function \verb!wavenumbers_angles! has been written that takes as input a set of incident and
adjoint angles and generates the points of the wavenumber cloud using formula
\eqref{eq:wvnb_angles}. To find the acquisition layout on the surface that would produce the
angles a function \verb!layout2angles! converts the positions $(x, z)$ of a
source or receiver to an angle $\phi$ via the formula 

\begin{equation*}
	\phi = \arccos\left( \frac{z_{\x} - z}{\sqrt{ \left(x_{\x} - x\right)^2 + \left(
	y_{\x} - y \right)^2 } } \right) \, ,
\end{equation*}

where $(x_{\x}, z_{\x})$ are the coordinates of the diffraction point $\x$. The
function is mainly used for convert initial acquisition layouts into angles to
feed them to the optimization program. The
function $angles2layout$ does the opposite via the formulae

\begin{equation*}
	x  = x_{\x} + z_{\x} \tan\left(  \phi \right) \quad \text{ and } \quad z  = 0
	\, ,
\end{equation*}

and is used to retrieve the positions of the sources and receivers on the
surface corresponding to the optimal angles at the end of the execution of the
program.

The wavenumber cloud of an acquisition layout is obtained by running
\verb!wavenumbers_angles! with the
angles returned by \verb!layout2angles! as input. Plot functions produces the wavenumber
clouds and acquisition layout plots like the ones seen in figures of section \ref{sec:wvnbs}.

\section{Voronoi tessellation}

 The function \verb!scipy.spatial.Voronoi! takes as input an
 array of points and generates
 Voronoi tessellations of the space $\R^N$ using the points as seeds. Because the
 function does not consider a domain it only provides vertices of the Voronoi
 regions that are intersections of ridges of the regions, the rest are considered
 stretching to infinity \ref{fig:voronoi1}. 

 To complete the Voronoi diagram a
 Python script has been used \cite{colorized_voronoi}, that reconstructs the infinite Voronoi regions
 outputted by \verb!scipy.spatial.Voronoi! in 
 2D into a finite diagram \ref{fig:voronoi2} but not inside a desired domain as
 it constructs the missing vertices by intersecting the ridges stretching to
 infinity with a circle the radius of which is a parameter to fix. 

 To get the Voronoi tessellation of a domain we define or approximate the
 latter by a
 polygon and we use the \verb!intersection! method made available by Shapely
 \cite{shapely}, a Python
 library for manipulating planar geometric object; we create polygonal Voronoi regions
 using the previous script with a sufficiently large radius in the option and we intersect them with
 the domain \ref{fig:voronoi3}. Concerning 
 the domain $\W$ in the wavenumber space, the circular arcs that compose it have been
 approximated
 by a large number of line segments.

\begin{figure}[h]
\centering
\subfigure[Voronoi diagram produced by Scipy]{
	\includegraphics[width = 0.3\textwidth]{voronoi1.png}
\label{fig:voronoi1}
}
%\hspace{4mm}
\subfigure[Using Virtanen's script.]{
	\includegraphics[width = 0.3\textwidth]{voronoi2.png}
\label{fig:voronoi2}
}
\subfigure[Intersection with the domain via Shapely.]{
	\includegraphics[width = 0.3\textwidth]{voronoi3.png}
\label{fig:voronoi3}
}
\caption{Creation process of a Voronoi tessellation of a domain.}
\label{fig:voronoi_python}
\end{figure}

Lloyd's method has been implemented for the CVT and has been used to
check the correct implementation of the variational approach, as discussed in
section \ref{sec:optimization}.

\section{Integration over polygons}

To compute the area of a Voronoi cell as in \eqref{eq:mi}, we use the divergence theorem to
get

\begin{equation}
	m_i = \int_{\Omega_i} \,dx\, dy = \frac{1}{2} \int_{\Omega_i} \nabla \cdot
	\begin{bmatrix} x \\ y \end{bmatrix} \,dx\, dy = \frac{1}{2} \int_{\partial \Omega_i}
	\begin{bmatrix} x \\ y \end{bmatrix} \cdot \vec{n} \,ds \, .
	\label{eq:area}
\end{equation}

Considering the cell to be a polygon we can parametrize the line segments
$C$
made by two adjacent vertices $p_1$ and $p_2$ by $C(t) : t \longmapsto t p_1 +
\left( 1 - t \right) p_2 $ for $t \in \left[ 0, 1 \right]$. Summing over the
line segments $C$
forming the polygon we get the area via the line integrals

\begin{equation}
	m_i = \frac{1}{2} \sum_C \int_0^1 \left[ \left( 1 - t \right) p_1 +  t p_2
	\right] \cdot \vec{n} \, \| C \| \,dt	\, .
	\label{}
\end{equation}

We apply the same technique and the same parametrization to get a way to
numerically evaluate $T ( X) $ and $F( X )$; we find for the centroid in \eqref{eq:mass}
\begin{equation}
	c_i 
	= \frac{1}{m_i} \int_{\Omega_i} \x \,d\x 
	= \frac{1}{m_i} \left[ \begin{aligned}\int_{\Omega_i} x \, dx \, dy \\
				\int_{\Omega_i} y \, dx \, dy\end{aligned}\right]
	= \frac{1}{2 m_i} \left[ \begin{aligned}\int_{\Omega_i} \nabla \cdot
			\begin{bmatrix} x^2 \\ 0 \end{bmatrix} \, dx \, dy \\
		\int_{\Omega_i} \nabla \cdot \begin{bmatrix} 0 \\ y^2 \end{bmatrix} \, dx \, dy\end{aligned}\right]
	= \frac{1}{2 m_i} \left[ \begin{aligned}\int_{\partial \Omega_i}
			\begin{bmatrix} x^2 \\ 0 \end{bmatrix} \cdot \vec{n} \, ds  \\
		\int_{\partial \Omega_i} \begin{bmatrix} 0 \\ y^2 \end{bmatrix} \cdot
	\vec{n} \, ds \end{aligned}\right] \, .
	\label{eq:centroid}
\end{equation}
 
For the energy function  \eqref{eq:F}, denoting $\x_i$ the seed of the
Voronoi cell $\Omega_i$, we would have

\begin{equation}
	\int_{\Omega_i} \| \x - \x_i \|^2 \,d\x 
	= \int_{\Omega_i} \| \x\|^2 + 2\, \x \cdot
	\x_i + \| \x_i \|^2 \,d\x 
	= \int_{\partial \Omega_i} \left( \frac{1}{3} \begin{bmatrix}
				x^3 \\ y^3 \end{bmatrix} + \begin{bmatrix} x_i x^2 \\ y_i
				y^2 \end{bmatrix} + \frac{\| \x_i \|}{2} \begin{bmatrix} x \\ y
			\end{bmatrix} \right)\cdot \vec{n} \,ds .
	\label{eq:integralF}
\end{equation}

The function \verb!scipy.integrate.quad! has been used to compute the line integrals. 

\begin{table}[h]
	\centering
	\begin{tabular}{c|c|c|c|c|c|}
		\cline{2-6}
		 & \multicolumn{5}{|c|}{Shapes} \\
		\cline{2-6}
		& Triangle & Square & Hectagon & L-shaped & $\W$ \\
		\hline
		\multicolumn{1}{|c|}{number of seeds} & $56$  & $100$ &
		$95$ & $118$ & $99$ \\
		\hline
		\multicolumn{1}{|c|}{$c_i$} & $3\times 10^{- 15}$  & $3.3\times 10^{- 15}$ &
		$1.96\times 10^{- 15}$ & $5.23\times 10^{- 15}$ & $2.37\times 10^{- 14}$ \\
		\hline
		\multicolumn{1}{|c|}{$m_i$} & $5.36\times 10^{- 15}$ & $2.99\times 10^{-
		15}$ &  $3.92\times 10^{- 15}$ & $5.67\times 10^{- 15}$ & $2.62\times 10^{- 14}$ \\
		\hline
	\end{tabular}
	\caption{Relative errors}
	\label{tab:integrals1}
\end{table}

To test the correct implementation of formulae \eqref{eq:area} and
\eqref{eq:centroid} we generate a Voronoi tessellation of some polygonal domain
using random seeds and we compute the masses and centroids of the 
Voronoi cells using the line integrals and compare the result with the methods
\verb!area! and \verb!centroid! of Shapely. Table \ref{tab:integrals1} shows the
relative error for different domains. 


\section{Optimization \label{sec:optimization}}

Functions $F$ and $K$ have been implemented along with the gradients $\nabla F$
and $\nabla \left( F_{\phi}\right)$. The gradient is a fundamental component of
a gradient descent method 
and it is
useful to test its correct implementation. For that we will compare the
implementations with approximations given by finite difference methods, using
both a first
order right-sided scheme

\begin{equation}
	\frac{\partial f}{\partial x_i} (\x) \approx  \frac{ f(\x + h \cdot e_i) -
	f(\x) }{h} ,  i = 1, \ldots, n \, ,
	\label{eq:1st-scheme}
\end{equation}

and a second-order centered scheme 

\begin{equation}
	\frac{\partial f}{\partial x_i} (\x) \approx  \frac{ f(\x + h \cdot e_i) - f(\x - h
	\cdot e_i) }{2 h},   i = 1, \ldots, n \, ,
	\label{eq:2nd-scheme}
\end{equation} 

for $f$ a continuous scalar-valued function of several variables on $\R^N$ and $h$ a small positive increment with
$e_i$ denoting the $i$th standard unit vector of $\R^N$. The CVT energy function $F$ and its gradient will be
evaluated at a set $X$ composed of randomly generated points inside
some polygon; the polygon will play the role
of the
domain and the points will be the seeds which will generate its Voronoi
tessellation. The cost function $F_{\phi}$ will be valued at
randomly generated sets of incident and adjoint angles spanning $[ - \phi_{max},
\phi_{\max}]$. For the comparison we will compute the relative error of the
finite difference methods at some point $\x$ 

\begin{equation*}
	\epsilon = \frac{\| \nabla f(\x) - \widetilde{ \nabla f } \|}{\| \nabla f(\x)
	\|} \, , 
\end{equation*}

where $\widetilde{ \nabla f }$ is the approximated gradient obtained through
\eqref{eq:1st-scheme} or \eqref{eq:2nd-scheme}.

Figure \ref{fig:gradient_F} shows the
relative errors obtained using different values of the increment $h$ with $F$
evaluated at a tessellation of $\W$ and \ref{fig:gradient_F-phi} shows the
relative error for $F_{\phi}$ at random angles. We confirm that the right-sided
scheme is of first order and the centered scheme is of second order which
indicates that the implemented gradients are correctly derived from their respective
functions.

\begin{figure}[h]
\centering
\subfigure[Test $\nabla F$ on $\W$.]{
	\includegraphics[height = 0.7\textwidth]{gradient_F.png}
\label{fig:gradient_F}
}
\subfigure[Test $\nabla F_{\phi}$ on a random layout.]{
	\includegraphics[height = 0.7\textwidth]{gradient_F-phi.png}
\label{fig:gradient_F-phi}
}

\caption{Relative errors of the gradients with first and second order finite
difference schemes. The function $F$ has been evaluated on $\W$ at $24$
random points used as seeds and $F_{\phi}$ has been evaluated at an 
acquisition layout composed of $7$ sources and $7$
receivers of random angles $\phi_s$ and $\phi_r$.}
\label{fig:test_gradients}
\end{figure}

Being confident about the cost functions and their gradients we can use an
optimization method to solve the corresponding problems. For that we will use
\verb!scipy.optimize.minimize!, a function for minimizing scalar objective
functions of one or mor evariables, proposing several solvers among them \verb!BFGS! for
unconstrained problems and \verb!L-BFGS-B! for bounded problems; the former will
be used for the CVT as there is no clear way to identify the bounds
characterizing the seeds and both solvers for the wavenumber coverage problem.

\begin{table}[h]
	\centering
	\begin{tabular}{c|c|c|c|c|c|}
		\cline{2-6}
		 & \multicolumn{5}{|c|}{Shapes} \\
		\cline{2-6}
		& Triangle & Square & Hectagon & L-shaped & $\W$ \\
		\hline
		\multicolumn{1}{|c|}{number of seeds} & $49$  & $100$ &
		$94$ & $138$ & $105$ \\
		\hline

		\multicolumn{1}{|c}{} & \multicolumn{5}{c|}{\textbf{Initialization}} \\
		\hline
		\multicolumn{1}{|c|}{Value of $F$} & $2.15 \times 10^{-3}$  & $1.03$ &
		$3.32$ & $0.06$ & $12.22$ \\
		\cline{2-6}
		\multicolumn{1}{|c|}{Norm of $\nabla F$} & $0.22 \times 10^{- 3}$  & $0.59$ &
		$1.60$ & $0.06$ & $5.06$ \\
		\hline

		\multicolumn{1}{|c}{} & \multicolumn{5}{c|}{\textbf{Lloyd}} \\
		\hline
		\multicolumn{1}{|c|}{Value of $F$} & $8.49\times 10^{- 4}$  & $0.42$ &
		$1.13$ & $0.03$ & $3.53$ \\
		\cline{2-6}
		\multicolumn{1}{|c|}{Norm of $\nabla F$} & $5.27 \times 10^{-6}$  & $1.35
		\times 10^{-3}$ &
		$1.98\times 10^{- 4}$ & $8.5\times 10^{-5}$ & $3.77\times 10^{- 3}$ \\
		\cline{2-6}
		\multicolumn{1}{|c|}{Number of iterations} & $86$  & $163$ &
		$555$ & $308$ & $312$ \\
		\cline{2-6}
		\multicolumn{1}{|c|}{Execution time} & $1.24$ s  & $5.21$ s &
		$14.43$ s & $11.56$ s & $12.06$ s \\
		\hline

		\multicolumn{1}{|c}{} & \multicolumn{5}{c|}{\textbf{BFGS}} \\
		\hline
		\multicolumn{1}{|c|}{Value of $F$} & $8.43 \times 10^{-4}$  & $0.42$ &
		$1.13$ & $0.03$ & $3.53$ \\
		\cline{2-6}
		\multicolumn{1}{|c|}{Norm of $\nabla F$} & $3.33\times 10^{- 5}$  & $3.97\times 10^{- 5}$ &
		$5.35\times 10^{- 5}$ & $5.49\times 10^{- 5}$ & $4.46\times 10^{- 5}$ \\
		\cline{2-6}
		\multicolumn{1}{|c|}{Number of iterations} & $146$  & $153$ &
		$164$ & $302$ & $133$ \\
		\cline{2-6}
		\multicolumn{1}{|c|}{Number of $F$ evaluations} & $148$  & $154$ &
		$165$ & $304$ & $139$ \\
		\cline{2-6}
		\multicolumn{1}{|c|}{Execution time} & $99$ s & $163$ s &
		$170$ s & $466$ s & $233$ s \\
		\hline

	\end{tabular}
	\caption{Comparison of the results between the Lloyd's and BFGS methods.}
	\label{tab:Lloyd_BFGS}
\end{table}

We test the \verb!BFGS! solver of the \verb!scipy! library on instances of the CVT
problem against Lloyd's method. We take some different geometric shapes of
domains and generate random seeds inside it as an initialization and apply both
Lloyd's and BFGS methods to find a CVT. We then compare the results (values of $F$
and the $2$-norm of its gradient) and the performance (total execution time and
number of iterations) of each. 

Results of some of the tests have been compiled in table \ref{tab:Lloyd_BFGS}.
The stopping criteria for Lloyd's method have been chosen to be the residuals between the seeds and
the centroids of their respective Voronoi cells not exceeding a threshold,
namely $\| X - T(X) \| < 10^{-
6}$, or the number of iterations going beyond the limit $1000$; thus the method
either yields a CVT or stops short of it because it takes too much time to do
so. So far in the examples Lloyd's method always terminate with a CVT. We notice
that the two methods provide the same values of $F$ while the difference resides
in the magnitude of $\nabla F$, with BFGS always yielding a gradient of order
$10^{- 5}$ because of its stopping criterion which sets a threshold for the
gradient. This indicates that the two methods are both successful and the
optimization approch is correct and well implemented.
