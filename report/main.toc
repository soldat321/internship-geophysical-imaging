\contentsline {chapter}{Abstract}{i}{chapter*.1}% 
\contentsline {chapter}{Acknowledgements}{ii}{chapter*.2}% 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}% 
\contentsline {chapter}{\numberline {2}Background }{2}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Full Waveform Inversion}{2}{section.2.1}% 
\contentsline {section}{\numberline {2.2}Wavenumber content of the Gradient}{4}{section.2.2}% 
\contentsline {chapter}{\numberline {3}Optimization of the wavenumber space coverage }{6}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Study of the domain of the wavenumber space coverage }{6}{section.3.1}% 
\contentsline {subsection}{\numberline {3.1.1}Size of the Area}{7}{subsection.3.1.1}% 
\contentsline {subsection}{\numberline {3.1.2}Number of points }{8}{subsection.3.1.2}% 
\contentsline {subsection}{\numberline {3.1.3}Regularity }{9}{subsection.3.1.3}% 
\contentsline {section}{\numberline {3.2}Centroidal Voronoi Tessellation}{10}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}Geometric characterization}{11}{subsection.3.2.1}% 
\contentsline {subsection}{\numberline {3.2.2}Variational characterization}{12}{subsection.3.2.2}% 
\contentsline {section}{\numberline {3.3}Regularity of the wavenumber coverage as an instance of CVT }{14}{section.3.3}% 
\contentsline {chapter}{\numberline {4}Implementation }{16}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Wavenumbers}{16}{section.4.1}% 
\contentsline {section}{\numberline {4.2}Voronoi tessellation}{16}{section.4.2}% 
\contentsline {section}{\numberline {4.3}Integration over polygons}{17}{section.4.3}% 
\contentsline {section}{\numberline {4.4}Optimization }{18}{section.4.4}% 
\contentsline {chapter}{\numberline {5}Results }{20}{chapter.5}% 
\contentsline {section}{\numberline {5.1}A first optimization of the layout }{20}{section.5.1}% 
\contentsline {section}{\numberline {5.2}CVT of $\mathcal {W}$}{20}{section.5.2}% 
\contentsline {section}{\numberline {5.3}Different initializations}{21}{section.5.3}% 
\contentsline {chapter}{\numberline {6}Conclusion}{24}{chapter.6}% 
\contentsline {chapter}{\numberline {A}Optimization}{25}{appendix.A}% 
\contentsline {chapter}{\numberline {B}Adjoint state method }{26}{appendix.B}% 
\contentsline {chapter}{Bibliography}{27}{appendix*.26}% 
