import numpy as np
from scipy import integrate
import matplotlib.pyplot as plt

def construct_discrete_signal_centered_at_0(dt, N, func):
    # dt : sample spacing (distance between two adjacent samples)
    # N : half of the sample size or number of positive frequencies
    
    double_N = 2*N
    
    # The time sample
    T = np.linspace(- double_N*dt/2, double_N*dt/2, double_N) 
    # The signal sample
    x = np.array( [func(t) for t in T] ) 
    
    return x, T

def construct_discrete_signal_starting_from_0(dt, N, func):
    # dt : sample spacing (distance between two adjacent samples)
    # N : half of the sample size or number of positive frequencies
    
    double_N = 2*N
    
    # The time sample
    T = np.linspace(0, double_N*dt, double_N) 
    # The signal sample
    x = np.array( [func(t) for t in T] ) 
    
    return x, T

def plot_signals(Xs, Signals, Labels, title, xLabel, yLabel):
    plt.figure(figsize = (20, 10))
    
    plt.title(title)
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    
    for i in range(len(Signals)):
        plt.plot(Xs[i], Signals[i], label = Labels[i])
    
    plt.legend()
    plt.show()

def retrieve_frequencies(x, N, dt):
    # x : signal
    # N : number of positive frequencies
    
    double_N = 2*N

    X_noshift = np.fft.fft(x) # The amplitudes
    X = np.fft.fftshift(X_noshift) # The reordered amplitudes
    Xi = np.fft.fftshift( np.fft.fftfreq(double_N, dt) ) # The frequencies sample 
    
    return X_noshift, Xi, X

def extract_an_bn(X, N):
    # an indices of the Fourier series
    A = []
    for i in range(N):
        an = X[N + i] + X[N - i]
        A.append(an)
    A = np.array(A)

    # bn indices of the Fourier series
    B = []
    for i in range(N):
        bn = X[N - i] - X[N + i]
        B.append(np.imag(bn))
    B = np.array(B)
    
    return A, B

def get_cn(N, dt, Freqs, func, T):
    C = []
    h = lambda t, f : func(t)*np.exp( - 1j*2*np.pi*f*t)
    if T[0] == 0.:
        P = 2*N*dt
        for freq in Freqs:
            cn = 1/P*integrate.quad(h, 0, P, args = (freq,))[0]
            C.append(cn)
    else:
        P = N*dt
        for freq in Freqs:
            cn = 1/P*integrate.quad(h, - P, P, args = (freq,))[0]
            C.append(cn)
    C = np.array(C)
    
    return C
