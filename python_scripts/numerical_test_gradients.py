import numpy as np
import matplotlib.pyplot as plt
from Wavenumbers_components import *
from colorized_voronoi import *
from cvt import *
from cvt_bfgs import *
from wavenumers_bfgs import *

#f: function
#grad_f: known gradient of the function
#x: R^n point at which to assess value of the gradient 
#args: list of options of the function
#h: the step 
#order: order of the scheme, either 1 (first-order right-sided) or 2 (second-order centered) 
#error: True - returns relative error; False - returns vector of the numerical gradient
def numerical_test_gradient_coordinates(f, grad_f, x, h = 1e-4, args = (), order = 1, error = True):
    n = x.shape[0] # number of components in the variable
    numerical_grad = np.zeros(x.shape) # the numerical gradient of the function at x; to which compare our gradient function
    for i in range(n):
        ei = np.zeros(x.shape)
        ei[i] = 1 # the ith vector of the basis of R^n
        if order == 1:
            numerical_grad[i] = ( f(x + h*ei, *args) - f(x, *args) )/h # the ith component of the numerical gradient
        else:
            numerical_grad[i] = ( f(x + h*ei, *args) - f(x - h*ei, *args) )/(2*h)
    grad = grad_f(x, *args) # value of the grdient function at x
    if error:
        return np.linalg.norm(numerical_grad - grad)/np.linalg.norm(grad)
    else:
        return numerical_grad

#f: function
#grad_f: known gradient of the function
#x: R^n point at which to assess value of the gradient 
#args: list of options of the function
#h: the step 
#d: the direction; if not specified then it is srandomly generated
# https://timvieira.github.io/blog/post/2017/04/21/how-to-test-gradient-implementations/
def numerical_test_gradient_directional(f, grad_f, x, h = 1e-4, args = (), order = 1, d = None):
    n = x.shape[0]
    grad = grad_f(x, *args)
    
    if d == None:
        d = np.random.normal(0, 1, size = n) #the random direction
    d = d/np.linalg.norm(d) # normalization
    if order == 1:
        approximation = (f(x + h*d, *args) - f(x, *args))/h
    else:
        approximation = (f(x + h*d, *args) - f(x - h*d, *args))/(2*h)
    return np.linalg.norm( np.dot(grad, d) - approximation )/np.linalg.norm(grad)

def plot_gradient_error(f, grad_f, x, args = (), **kwargs):
    n = 10
    exponents = np.linspace( -7, -1, n) # exponents of the steps h
    h = 10**(exponents) # the steps
    errors = np.zeros(n)
    errors2 = np.copy(errors)
    errors3 = np.copy(errors)
    errors4 = np.copy(errors)
    for i in range(n):
        #errors[i] = numerical_test_gradient_coordinates(f, grad_f, x, h = h[i], args = args)
        errors2[i] = numerical_test_gradient_coordinates(f, grad_f, x, h = h[i], args = args, order = 2)
        #errors3[i] = numerical_test_gradient_directional(f, grad_f, x, h = h[i], args = args)
        #errors4[i] = numerical_test_gradient_directional(f, grad_f, x, h = h[i], args = args, order = 2)
    plt.figure(figsize = (10, 20))
    ax = plt.gca()
    ax.set_aspect('equal')
    #ax.loglog(h, errors, label = "1st order scheme")
    ax.loglog(h, errors2, label = "2nd order scheme")
    ax.loglog(h, h**2, label = "$h^2$")
    #ax.loglog(h, errors3, label = "directional derivative, 1st order")
    #ax.loglog(h, errors4, label = "directional derivative, 2nd order")
    if 'title' in kwargs.keys():
        plt.title(kwargs['title'])
    plt.legend()
    plt.show()
