import numpy as np

##################################################
# Compute wavenumbers
##################################################

def wavenumber_ps_pr(x, s, r, k0):
    """
    Compute the wavenumber associated to a pair source-receiver (s, r)
    using formula: k = k0 × (ps + pr)
    
    Parameters
    ----------
        x : coordinates of the diffraction point
        s : coordiantes of the source
        r : coordinates of the receiver
        k0 : value of the initial wavenumber

    Returns
    ----------
        k : the wavenumber
    """

    # direction (unit) vector of the ray connecting s to x
    ps = x - s
    ps = ps/np.linalg.norm(ps)  

    # direction (unit) vector of the ray connecting r to x
    pr = x - r
    pr = pr/np.linalg.norm(pr)

    return k0*( ps + pr )

def wavenumbers_positions_formula(x, S, R, c0, f0):
    """
    A function that computes the wavenumbers associated to a set of sources and receivers
    using the vectors ps and pr: k = k0 × (ps + pr)

    Parameters
    ----------
        x : coordinates of the diffraction point
        S : a n×2 matrix containing the coordiantes of the sources
        R : a n×2 matrix containing the coordiantes of the receivers
        c0 : constant velocity of the medium
        f0 : time frequency

    Returns
    ----------
        K : a 2×n matrix contaning the coordinates of the wavenumbers
    """

    K = [] # a list containing the wavenumbers
    k0 = (2*np.pi*f0)/c0

    for s in S: 
        for r in R: 
            k = wavenumber_ps_pr(x, s, r, k0)
            K.append(k)
    
    # Transform the list into a n×2 matrix
    n = S.shape[0]*R.shape[0]
    K = np.array(K).reshape(n, 2) 

    return K

# Function that computes the components of a wavenumber using the angular formula (angles $\phi_s$ and $\phi_r$)
def wavenumber_angles(phis, phir, k0):
    """
    Compute the wavenumber associated to a pair incident
    and adjoint angles (phis, phir) using the angular formula
    
    Parameters
    ----------
        phis : value of the incident angle
        phir : value of the adjoint angle
        k0 : value of the initial wavenumber

    Returns
    ----------
        k : the wavenumber
    """

    phi = (phis + phir)/2
    theta = phis - phir
    kx = 2*k0*np.cos(theta/2)*np.sin(phi)
    kz = - 2*k0*np.cos(theta/2)*np.cos(phi)

    return np.array([kx, kz])

def wavenumbers_angular_formula(PhiS, PhiR, c0, f0):
    """
    A function that computes the wavenumbers associated to a set of incident and adjoint angles
    using the angular formula of the wavenumbers

    Parameters
    ----------
        PhiS : an array containing the values of the incident angles
        PhiR : an array containing the values of the adjoint angles
        c0 : constant velocity of the medium
        f0 : time frequency

    Returns
    ----------
        K : a 2×n matrix contaning the coordinates of the wavenumbers
    """

    K = [] # a list containing the wavenumbers
    k0 = (2*np.pi*f0)/c0

    for phis in PhiS: 
        for phir in PhiR: 
            k = wavenumber_angles(phis, phir, k0)
            K.append(k)
    
    # Transform the list into a n×2 matrix
    n = PhiS.shape[0]*PhiR.shape[0]
    K = np.array(K).reshape(n, 2) 

    return K

def extract_coordinates_from_matrix(K):
    """
    A function to get the arrays of x and z components that constitute
    a matrix of wavenumber points

    Parameters
    ----------
        K : a n×2 maatrix containing the coordinates of the wavenumbers

    Returns
    ----------
        Kx : the x coordinates of the points
        Kz : the z coordinates of the points
    """

    Kx = K[:,0]
    Kz = K[:,1]

    return Kx, Kz

##################################################
# Functions for the convertion of positions to angles and vice-versa
##################################################

# x is the diffraction point
def layout2angles(x, S, R):
    """
    A function that computes the values of the incident and adjoint angles associated to an acquisition layout

    Parameters
    ----------
        S : a n×2 matrix containing the coordiantes of the sources
        R : a n×2 matrix containing the coordiantes of the receivers
        x : coordinates of the diffraction point

    Returns
    ----------
        PhiS : an array containing the values of the incident angles
        PhiR : an array containing the values of the adjoint angles
    """

    e2 = np.array([0, -1]) # downward direction of the vertical axis

    PhiS = []
    PhiR = []

    # Remark: all the vectors are unit vectors so no need to divide by the norms
    for s in S:
        ps = x - s
        ps = ps/np.linalg.norm(ps)  # unit vector in the direction of the rays connecting x to s
        # We check if the angle of ps is negative or positive compared to the reference direction
        # before computing it 
        if( ps[0] >= e2[0]):
            phis = np.arccos(np.inner(ps, e2)) # angle made by ps with the vertical, i.e. vector (0, -1)
        else:
            phis = - np.arccos(np.inner(ps, e2))
        PhiS.append(phis)
        
    for r in R:
        pr = x - r
        pr = pr/np.linalg.norm(pr) # unit vector in the direction of the rays connecting x to r
        # We check if the angle of pr is negative or positive compared to the reference direction
        # before computing it
        if( pr[0] >= e2[0]):
            phir = np.arccos(np.inner(pr, e2)) # angle made by ps with the vertical, i.e. vector (0, -1)
        else:
            phir = - np.arccos(np.inner(pr, e2)) 
        PhiR.append(phir)
    
    PhiS = np.array(PhiS)
    PhiR = np.array(PhiR)
    
    return PhiS, PhiR

# A function that computes the layout of the sources S and receivers R from the angles phi_s and phi_r
# x is the diffraction point
def angles2layout(PhiS, PhiR, x):
    """
    A function that computes positions of the sources and receivers at the surface from the values of the incident and adjoint angles

    Parameters
    ----------
        PhiS : an array containing the values of the incident angles
        PhiR : an array containing the values of the adjoint angles
        x : coordinates of the diffraction point

    Returns
    ----------
        S : a n×2 matrix containing the coordiantes of the sources
        R : a n×2 matrix containing the coordiantes of the receivers
    """

    Sx = []
    Sz = np.zeros(PhiS.shape[0]) # the sources will be situated on the surface so the z component is null
    for phis in PhiS:
        sx = x[0] + x[1]*np.tan(phis)
        Sx.append(sx)
    
    Rx = []
    Rz = np.zeros(PhiR.shape[0]) # the sources will be situated on the surface so the z component is null    
    for phir in PhiR:
        rx = x[0] + x[1]*np.tan(phir)
        Rx.append(rx)

    S = np.array([Sx, Sz]).T
    R = np.array([Rx, Rz]).T
    
    return S, R
