import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import Voronoi, voronoi_plot_2d
import matplotlib.path as mplPath
from shapely.geometry import Polygon
from shapely.geometry import Point
import quadpy
from cvt import *

from scipy.integrate import quad

def integrand_surface(t, p, q, normal):
    return np.dot(t*p + (1 - t)*q, normal)*np.linalg.norm(p - q)

def integrand_x2(t, p, q, normal):
    return np.dot((t*p + (1 - t)*q)**3, normal)*np.linalg.norm(p - q)

def integrand_x(t, p, q, normal, c):
    return np.dot(np.multiply(c, (t*p + (1 - t)*q)**2), normal)*np.linalg.norm(p - q)

def normale(polygon, p1, p2):
    P = Polygon(polygon)
    n1 = np.dot(np.array([[0, -1], [1, 0]]), p2 - p1) # Attention à la direction!: dans le sens des aiguilles d'une montre
    n2 = np.dot(np.array([[0, 1], [-1, 0]]), p2 - p1) # Attention à la direction!: dans le sens des aiguilles d'une montre
    middle = .5*(p1 + p2)
    epsilon = 0.001
    if P.contains(Point(middle + epsilon*n1)):
        n = n2/np.linalg.norm(n2)
    else:
        n = n1/np.linalg.norm(n1)
    return n

def area(polygon):
    Sm = 0
    for i in range(len(polygon) - 1):
        p1 = polygon[i]
        p2 = polygon[i + 1]
        n = normale(polygon, p1, p2)
        Sm = Sm + quad(integrand_surface, 0, 1, args = (p1, p2, n))[0]
    return 0.5*Sm

def centroid(polygon):
    Sx = 0
    Sy = 0
    cx = np.array([1, 0])
    cy = np.array([0, 1])
    for i in range(len(polygon) - 1):
        p1 = polygon[i]
        p2 = polygon[i + 1]
        n = normale(polygon, p1, p2)
        Sx = Sx + 0.5*quad(integrand_x, 0, 1, args = (p1, p2, n, cx))[0]
        Sy = Sy + 0.5*quad(integrand_x, 0, 1, args = (p1, p2, n, cy))[0]
    m = area(polygon)
    return np.array([Sx, Sy])/m

def integral_polygon(polygon, seed):
    S = 0
    for i in range(len(polygon) - 1):
        p1 = polygon[i]
        p2 = polygon[i + 1]
        n = normale(polygon, p1, p2)
        S = S + 0.5*np.linalg.norm(seed)**2*quad(integrand_surface, 0, 1, args = (p1, p2, n))[0] - quad(integrand_x, 0, 1, args = (p1, p2, n, seed))[0] + 1/3*quad(integrand_x2, 0, 1, args =(p1, p2, n))[0]
    return S

def F(X, domain):
    seeds, cells = voronoi_domain(X, domain)
    S = 0
    for i in range( len(cells) ):
        S = S + integral_polygon(cells[i], seeds[i])
    return S

def grad_F(X, domain):
    seeds, cells = voronoi_domain(X, domain)
    grad = np.zeros( (len(seeds), 2) )
    for i in range( len(seeds) ):
        m_i = area(cells[i])
        c_i = centroid(cells[i])
        grad[i,:] = 2*m_i*(seeds[i] - c_i)
    return grad.flatten()

#====+++++==================+++++++++++++++++++++++++++++++++++++++===========

def F2(X, domain):
    seeds, cells = voronoi_domain(X, domain)
    S = 0
    for i in range( len(cells) ):
        #cell_centered = cells[i] - seeds[i]
        S = S + integral_polygon2(cells[i], lambda x: (x[0] - seeds[i, 0])**2 + (x[1] - seeds[i, 1])**2)
        #S = S + integral_polygon2(cells[i], seeds[i])
    return S

def integral_polygon2(polygon, f):
    #if not Polygon(polygon).contains(Point(x)):
    x = np.asarray( Polygon(polygon).centroid )
    # https://github.com/nschloe/quadpy#triangle-t2
    scheme = quadpy.t2.get_good_scheme(25)
    S = 0
    for i in range(len(polygon) - 1):
        triangle = np.array([x, polygon[i], polygon[i + 1]])
        S = S + scheme.integrate(f, triangle)
    return S

#def F_i(x):
#    return x[0]**2 + x[1]**2

