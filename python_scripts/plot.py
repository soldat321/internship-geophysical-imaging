import numpy as np
import matplotlib.pyplot as plt
from wavenumbers import extract_coordinates_from_matrix

#####################################################################
# Plot functions
#####################################################################

def plot_wavenumber_cloud(K, labels, **kwargs):
    """
    Plot the x and z components of the wavenumbers
    and get the wavenumber cloud

    Parameters
    ----------
        K : points to plot, either a n×2 matrix or a list of matrices
        labels : a string or a list of strings
        colors : either a string or a list of strings specifying the color option of pyplot 
        markers : either a string or a list of strings specifying the marker option of pyplot 
        title: a string that contains the title of the plot, to be passed to pyplot
    """

    plt.figure(figsize=(20, 20))
    ax = plt.gca()
    ax.set_aspect('equal', adjustable = 'box')

    plt.xlabel("$k_x$", fontsize = 'xx-large')
    plt.ylabel("$k_z$", fontsize ='xx-large')

    # 1. Input is a list
    if type(K) is list:
        N = len(K) # number of sets 
        colors = ["#1f77b4"]*N # Default color: steelblue
        markers = ["o"]*N # Default marker
        if 'colors' in kwargs.keys():
            colors = kwargs['colors']
        if 'markers' in kwargs.keys():
            markers = kwargs['markers']
        for i in range(N):
            X, Z = extract_coordinates_from_matrix(K[i]) # Extract the x and z components of the points
            marker = markers[i]
            color = colors[i]
            label = labels[i]
            ax.scatter(X, Z, marker = marker, color = color, label = label)

    # 2. Input is a unique cloud of points
    elif isinstance(K, np.ndarray):
        color = "#1f77b4" 
        marker = "o"
        if 'colors' in kwargs.keys():
            color = kwargs['colors']
        if 'markers' in kwargs.keys():
            marker = kwargs['markers']
        X, Z = extract_coordinates_from_matrix(K)
        ax.scatter(X, Z, marker = marker, color = color, label = labels)

    if 'title' in kwargs.keys():
        plt.title(kwargs['title'])

    plt.legend()
    plt.show()

def plot_acquisition_layout(S, R, x, **kwargs):
    """
    Plot the sources and receivers on the surface,
    along with the diffraction (target) point x in the subsurface

    Parameters
    ----------
        S : a n×2 matrix containing the coordinates of the sources
        R : a n×2 matrix containing the coordinates of the receivers
        x : the coordinates of the diffraction point
        limits : a 2×2 matrix containing the coordinates of the extremities of the acquisition range
        title: a string that contains the title of the plot, to be passed to pyplot
    """

    plt.figure(figsize=(20, 10))
    ax = plt.gca()
    ax.set_aspect('equal', adjustable = 'box')

    plt.xlabel("$x$", fontsize = 'x-large')
    plt.ylabel("$z$", fontsize ='x-large')

    ax.scatter(S[:,0], S[:,1], marker = "v", label = "sources")
    ax.scatter(R[:,0], R[:,1], marker = "^", label = "receivers")
    ax.scatter(x[0], x[1], marker = "X", label = "target point")

    if 'limits' in kwargs.keys():
        limits = kwargs['limits']
        ax.scatter(limits[:,0], limits[:,1], marker = "x", color = "r", label = "boundary of the acquisition range")
    if 'title' in kwargs.keys():
        plt.title(kwargs['title'])
    plt.legend()
    plt.show()
    
def plot_nearest_neighbours_pt_in_cloud(K, K_unique, index, List_true_nn, List_neighbours_indices):
    """
    A function that allows to vizualize the nearest neighbour of a point in the wavenumber cloud and compare it to the neighbours in the indices space. 

    Parameters
    ----------
    K : a n×2 matrix wavenumber cloud of points with possibly duplicated points
    K_unique : a m×2 matrix wavenumber cloud of points with no duplicated points (returned by the function find_nearest_neighbours), with m the number of unique points of K. This cloud is used to get the coordinates of the nearest neighbour.
    index: index of the point of interest
    List_true_nn : full list of the nearest neighbour indices associated to the points in K (returned by the function find_nearest_neighbours)
    List_neighbours_indices: list of the points that are neighbours in the indices space (returned by function min_indices_distance)
    """

    # nn: nearest neighbour (abbrev.)
    index_true_nn = List_true_nn[index] # What is the index in K of the nearest neighbour?
    nn_pt = K_unique[index_true_nn,:] # What is the point associted to the nearest neighbour?
    List = [K, K[index,:].reshape(1,2) , List_neighbours_indices[index], nn_pt.reshape(1,2)] # List of set of points
    plot_wavenumber_cloud(List, colors = ['steelblue', 'orange', 'darkred', 'limegreen'], labels = ["Wavenumbers", 'Point of index ' + str(index), 'Adjacent points in the indices', 'Found nearest neighbour'], markers = ['.', 'd', 'P', 'x'], title = "Point of index " + str(index) + " : its nearest neighbour and adjacent points in index space")

def histogram_distances(K, distances_unique):
    """
    A function that plots a hitogram of the minimum distances. 

    Parameters
    ----------
    distances_unique : list of the minimum distance of each unique point of K
    K : a n×2 matrix wavenumber cloud of points 
    """

    # The histogram of the closest distance
    plt.figure( figsize = (10, 5))
    ax = plt.gca()
    ax.hist(distances_unique, bins = 200)
    plt.title("Distribution of the distances between points and their nearest neighbour in the wavenumber cloud")
