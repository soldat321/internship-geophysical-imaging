import numpy as np
import matplotlib.pyplot as plt
import matplotlib.path as mplPath
from wavenumbers import wavenumber_angles
from colorized_voronoi import voronoi_finite_polygons_2d
from scipy.spatial import Voronoi, voronoi_plot_2d
from shapely.geometry import Polygon, Point

def reshape_into_matrix(array):
    """
    A function to reshape an array of size 1×(2n) into a matrix of size n×2 
    for converting a wavenumber array into wavenumber matrix

    Parameters
    ----------
        array : an array of size 2n alternating the x and z components of the wavenumbers

    Returns
    ----------
        K : the wavenumber matrix
    """

    K = array.flatten().reshape( int(array.shape[0]/2), 2)
    return K

def voronoi_domain(seeds, domain):
    """
    A function that returns the Voronoi tessellation of a domain

    Parameters
    ----------
        seeds : a n×2 matrix containing the coordinates of the seed points
        domain : a list of points that constitue the vertices of the polygon used as the domain

    Returns
    ----------
        seeds : the seeds of the tessellation 
            the same as the input but the points that are outside the domain are removed
        cells : the Voronoi cells, a list of polygons expressed via their vertices (regrouped in lists also)
    """

    # We construct the polygon of the domain
    polygon = Polygon(np.array(domain))
    
    # We keep only the seeds that are inside the domain
    # https://stackoverflow.com/questions/42305987/python-shapely-polygon-containspoint-not-giving-correct-answer-i-think
    # https://stackoverflow.com/questions/59417997/how-to-plot-a-list-of-shapely-points
    if seeds.ndim == 1:
        inside = reshape_into_matrix(seeds)
    else:
        inside = seeds
    # TO DO : check the usefulness of the below lines
    #points = [Point(p) for p in inside] # Shapely points
    #is_inside = [polygon.contains(p) for p in points]
    #inside = inside[is_inside]
    
    # We construct the unbounded Voronoi diagram from seeds
    vor = Voronoi(inside)
    
    # Copyright: Pauli Virtanen 
    # https://gist.github.com/pv/8036995
    # We assign a large value to radius in order to avoid voronoi cells not 
    # forming a partition of the domain
    cells, vertices = voronoi_finite_polygons_2d(vor, radius = polygon.length*100)

    new_cells = []
    for cell in cells:
        polygon_cell = Polygon(vertices[cell])
        intersection = polygon.intersection(polygon_cell)
        # To do : check if we still need the geom_type test
        # l-bfgs-b often gives seeds outside of the domain, so the intersection will be empty
        # if there is no test the program crashes because "GeometryCollection" has no exterior
        # so if the seed is outside (intersection is empty) we take the whole cell (the vertices of which are far far away)
        # and we will get a very large cost (value of objective fct) and this will "punish" l-bfgs-b from choosing 
        # a seed outside the domain and at the next iteration it won't do it again
        #if intersection.geom_type == "GeometryCollection":
        #    new_cells.append(np.asarray(polygon_cell.exterior.coords))
        #else:
        #    new_cells.append(np.asarray(intersection.exterior.coords))
    
    seeds = vor.points
    cells = new_cells

    return seeds, cells

def plot_voronoi_domain(seeds, domain, cells):
    """
    Function to plot a Voronoi tessellation of a domain

    Parameters
    ----------
        seeds : the seeds of the Voronoi tessellation, a n×2 array
        domain : a list of points that constitue the vertices of the polygon used as the domain
        cells : a list of polygons expressed via their vertices (regrouped in lists also)
    """
    
    plt.figure(figsize = (20, 20))
    ax = plt.gca()
    ax.set_aspect('equal')
    
    # We construct the polygon of the domain
    # https://stackoverflow.com/questions/55522395/how-do-i-plot-shapely-polygons-and-objects-using-matplotlib
    polygon = Polygon(np.array(domain))
    ax.plot(*polygon.exterior.xy, 'k-', label = "Domain boundary")
    
    centroids = np.zeros((len(cells), 2))
    i = 0
    for cell in cells:
        polygon_cell = Polygon(cell)
        ax.fill(*polygon_cell.exterior.xy, alpha = .8)
        centroids[i, :] = np.asarray(polygon_cell.centroid)
        i = i + 1
    ax.plot(centroids[:,0], centroids[:,1], 'rx', label = "Centroids")
    
    ax.plot(seeds[:,0], seeds[:,1], 'kP', label = "Seeds")
    
    plt.legend()
    plt.show()    

def Lloyd(initialization, domain):
    """
    Implementation of Lloyd's method. 
    Parameters
    ----------
        initialization : the initial seeds, a n×2 array
        domain : a list of points that constitue the vertices of the polygon used as the domain
    
    Returns
    ----------
        seeds : the seeds that generate the CVT of the domain
        cells : the Voronoi cells of the CVT, a list of polygons expressed via their vertices (regrouped in lists also)
    """

    # The initial seeds
    centroids = initialization

    stopping_criteria = False
    iterations = 0
    # the main loop
    while not stopping_criteria :
        iterations = iterations + 1

        # The Voronoi tesselation
        seeds, cells = voronoi_domain(centroids, domain)

        # Computing the centroid of the Voronoi cells
        centroids = []
        for cell in cells:
                centroid = np.array( Polygon(cell).centroid )
                centroids.append(centroid)
        centroids = np.array(centroids)

        # the minimum distance between the seeds and the centroids
        min_norm = np.min( np.linalg.norm(centroids - seeds, axis = 1) )

        stopping_criteria = ( min_norm > 10**(-6) ) & (iterations < 1000)

    return seeds, cells

def domain_construction(phi_max, k0):
    """
    A function to build the envelope of the wavenumber cloud 
    Parameters
    ----------
        phi_max : the maximum value of the incident and adjoint angles
        k0 : initial wavenumber

    Returns
    ----------
        envelope : a 2D array of the coordinates of the vertices constituing the polygonal approximation of the envelope 
    """

    # Computing the side circular arcs
    angles_centers = np.array([- phi_max, phi_max])
    angles_arcs = np.linspace(start = - phi_max, stop = phi_max, num = 100, dtype = float)

    side_Arcs = []
    for phis in angles_centers:
        # We first compute the center of the circular arc
        center = k0*np.array([np.sin(phis), - np.cos(phis)])
        for phir in angles_arcs: 
            # Computation of the x, z components
            k = center + k0*np.array([np.cos(phir - np.pi/2), np.sin(phir - np.pi/2)])
            side_Arcs.append(k)
    side_Arcs = np.array(side_Arcs)  

    # Computing the bottom circular arc constituted of wavenumbers obtained via superimposed sources and receivers
    bottom_Arc = []
    for phis in angles_arcs:
        phir = phis
        k = wavenumber_angles(phis, phir, k0)
        Superimposed.append(k)
    bottom_Arc = np.array(bottom_Arc)  

    side_Arcs = np.unique(side_Arcs, axis = 0) # No intersection between the two sides arcs
    B = np.flip(Superimposed, axis = 0)[1:-1,] # No intersection between the sides arcs and the borrom arc

    # We get rid og the two side horns tips to get a valid polygon (and be able to perfom intersections with other polygons) 
    # https://stackoverflow.com/questions/13062334/polygon-intersection-error-in-shapely-shapely-geos-topologicalerror-the-opera
    envelope = np.append(A[1:-1,:], B, axis = 0)
    return envelope
