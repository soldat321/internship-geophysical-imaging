\documentclass[10pt, xcolor = table]{beamer}

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage[utf8]{inputenc}
\usepackage{eso-pic}
\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}
    \usepackage{animate}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage[absolute,showboxes,overlay]{textpos}
\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}
\TPshowboxesfalse 
\usepackage{xspace}
    \usepackage{graphicx}
    \usepackage{xmpmulti}
\usepackage{wrapfig}
\usepackage{subfigure}
\usepackage[numbers]{natbib}
\usepackage{mdframed}
\usepackage[]{hyperref}
\usepackage{xcolor}
\usepackage{tikz}
\renewcommand<>\cellcolor[1]{\only#2{\beameroriginal\cellcolor{#1}}}
\theoremstyle{plain}
\newtheorem{thm}{Theorem}

\theoremstyle{definition}
\newtheorem{defi}{Definition}

\theoremstyle{remark}
\newtheorem{rmk}{Remark}
\newcommand{\x}{\mathrm{x}}

\graphicspath{ {images/} }

% New commands
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
\newcommand{\me}[1]{\todo{#1}}
\newcommand{\them}[1]{\todo[color = red]{#1}}
\newcommand{\y}{\mathrm{y}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\W}{\mathcal{W}}

\title[ Geophysical imaging ]{\small Optimal design for full waveform inversion: optimization of the wavenumber space coverage}
\author{  \centering Mohamed Amine \textsc{Abdellaziz}  }

\subtitle{}
\date{ \centering \today  \\ \vspace{.5 cm} \raggedleft {\bf Supervisors} :  Ludovic
\textsc{Métivier}  \\  Edouard \textsc{Oudet}  } 

\begin{document}

% Title 

\begin{frame}
  \begin{tikzpicture}[remember picture,overlay]
    \node[yshift= - 1 cm][xshift = 5 cm]{%
      \includegraphics[scale = 1.7 ]{logo_ensimag_uga.png}};
  \end{tikzpicture}
 \titlepage
\end{frame}

% Outline
\begin{frame}{Outline}
	\tableofcontents
\end{frame}

\section{Full Waveform Inversion}

% intro FWI
\begin{frame}{Full Waveform Inversion}
\begin{itemize}
	\item \textbf{Geophysical imaging} is a minimally destructive geophysical exploration
			technique that aims at recovering the characteristics of the Earth.
			\pause
		\item The Full Waveform Inversion is a geophysical imaging technique that
			iteratively reduces the misfit between \textbf<3>{observed data} and
			\textbf<3>{calculated data}.
			in order to update a \textbf<3>{model} of the subsurface.
	\end{itemize}
\end{frame}

% Observed data
\begin{frame}{Observed data $d_{obs}$}
	\begin{itemize}
		\item Observed data are obtained by surveying the subsurface using sources and
			receivers.
	\end{itemize}

\begin{figure}[h]
	\centering
	\includegraphics<2>[width = 0.9\textwidth]{surface.png}
	\includegraphics<3>[width = 0.9\textwidth]{sources.png}
	\includegraphics<4>[width = 0.9\textwidth]{receivers.png}
	%\caption{Placement of sources and receivers to probe an area of interest.}
\end{figure}

\end{frame}

\begin{frame}{Seismograms}
	\begin{figure}[h]
\centering

\subfigure[A seismic trace]{
	\includegraphics[height = 0.4\textwidth]{trace.png}
\label{fig:tracea}
}
\hspace{4mm}
\subfigure[Seismogram]{
	\includegraphics[height = 0.4\textwidth]{traces.png}
\label{fig:traceb}
}
\caption{A seismic trace and a seismogram
recording the arrival times (on the ordinate) of the waves emitted by a source
at respectively one receiver and several receivers.
In the seismogram, the abcissa corresponds to the distance of a receiver to the
associated source, or offset, and thus is a function of space and time.}
\label{fig:offset}
\end{figure}
\end{frame}

% calculated data
\begin{frame}{Calculated data $d_{cal}$}

	\begin{itemize}
		\item Suppose we have a model $m$ of the subsurface; the characteristic of
			the medium at a point $x$ of the subsurface is given by $m (x)$.
			\pause
		\item We can compute the wavefield $u_s[m] (x, t)$ by solving the wave
			equation
			
\begin{equation}
A(m) u_s(x, t) = \varphi_s (x, t) \, , 
\label{eq:pde}
\end{equation}

defined by the wave operator $A(m) = \left(
\frac{1}{m^2}\frac{\partial^2}{\partial t^2} - \Delta \right)$.

 \pause

\item We obtain the wavefields registered by the receivers using the restriction
	operator
	\begin{equation*}
		R [u](x,t) = \int_{\Omega} \delta ( y - x ) u(y, t) dy \, .
	\end{equation*}
	\end{itemize}

\end{frame}

% FWI minimization
\begin{frame}{FWI as a minimization problem}
 The FWI can be expressed as a PDE constrained optimization problem is:
\begin{equation}
	\begin{split} \min_{m \in \mathcal{M}} & f(m) = \qquad \frac{1}{2} \sum_{s = 1}^{N_s}
		\sum_{r = 1}^{N_r} \int_0^T | d_{cal, s}(x_r, t) - d_{obs, s}(x_r, t) |^2 dt
		\\
	\text{subject to:}  & \qquad A(m) u_s (x, t) = \varphi_s(x, t), \text{ for $s = 1, \dots, N_s$}  \\
	& \qquad d_{cal, s} (x, t) =  R [u_s] (x, t) , \text{ for $s = 1, \dots, N_s$ } \\
	\end{split}
			\label{eq:min}
\end{equation}	

	\pause

The \textbf{Full Waveform Inversion} is a seismic imaging technique that uses a
gradient descent method to solve \eqref{eq:min} and match the calculated data
obtained through a computer model of \eqref{eq:pde} and observations.

\end{frame}

% FWI scheme
\begin{frame}{FWI scheme}
\begin{figure}[h]
	\centering
	\includegraphics<1>[width = 0.9\textwidth]{FWI1.png}
	\includegraphics<2>[width = 0.9\textwidth]{FWI2.png}
	\includegraphics<3>[width = 0.9\textwidth]{FWI3.png}
	\includegraphics<4>[width = 0.9\textwidth]{FWI4.png}
	\includegraphics<5>[width = 0.9\textwidth]{FWI5.png}
	\includegraphics<6>[width = 0.9\textwidth]{FWI6.png}
	\caption{Illustration of the iterative process used in the FWI
		\cite{lecture_notes}.}
\end{figure}
\end{frame}

% gradient
\begin{frame}{Adjoint state method}

	How to compute the gradient?

	Using the \textbf{adjoint state method}. For each source $s$ the method requires solving two PDE's:

	\begin{itemize}
			\pause
		\item Incident equation : $ A(m) u(x, t) = \varphi(x, t)$.
			\pause
		\item Adjoint equation : $ A(m)^{T} \lambda(x, t) = R^{- 1} \left[ d_{obs} (x, t)
			- d_{cal} (x, t) \right]$.
	\end{itemize}

	\pause
	
	The gradient can be computed as the \textbf{zero-lag time correlation} between
	the incident and adjoint wavefields $u_s$ and $\lambda_s$:

	\begin{equation*}
	\nabla f (x) = 	\sum_{s = 1}^{N_s}  \left( \frac{\partial A (m)}{\partial m} 
u_s (x, t) \star \lambda_s (x, t)\right) (0)\, .
	\end{equation*}

\end{frame}

% approximation model
\begin{frame}{Plane wave approximation}

	We will use an approximation model of the subsurface and suppose the
	following:

	\begin{itemize}
			\pause
		\item The medium is homogeneous with constant velocity $c_0$,
			\pause
		\item The sources and receivers are infinitely far from the diffraction
			point $\x$.
	\end{itemize}

	\pause

	The incident and adjoint wavefields can be expressed by plane waves:
	
\begin{equation*}
	\begin{aligned}
		u_s (x, t) & = e^{ i \frac{2 \pi f_0}{c_0} \left( p_s.x + t \right) } \, ,
		\\
		\lambda_r (x, t) & = e^{ i \frac{2 \pi f_0}{c_0} \left( p_r.x + t \right) }
		\, .
	\end{aligned}
\end{equation*}

 \pause

	The gradient can be approximated by

\begin{equation}
		\nabla f (x) \approx \sum_{s = 1}^{N_s} \sum_{r = 1}^{N_r} e^{ i \frac{2 \pi
		f_0}{c_0} \left( p_s + p_r \right).x } \, .
		\label{eq:approximation}
\end{equation}

\end{frame}

% Wavenumber cloud
\begin{frame}{Wavenumber coverage}


The wavenumber content of \eqref{eq:approximation} is given by the direction
vectors $p_s$ and $p_r$:

\begin{equation}
	k(s, r) = \frac{2 \pi f_0}{c_0} (p_s + p_r) = k_0 (p_s + p_r)
	\label{eq:ps+pr}
\end{equation}

\pause

	\begin{figure}[h]
		\centering
		\includegraphics[width = \textwidth]{wvnb_cloud.png}
		\caption{Wavenumber coverage with the target point $\x = (5000, - 3000)$, using $101$ sources and receivers positioned on the surface from $(0, 0)$
	to $(10000, 0)$ every $100$ m. The data are $c_0 = 2000$ m/s and we use $f_0 = 5$ Hz.}
		\label{fig:wavenumber_space}
	\end{figure}

\end{frame}

\begin{frame}{Objectif}
	\begin{itemize}
		\item We want the wavenumbers to \textbf{regularly} and \textbf{densely} cover an area as
			\textbf{large} as possible of the wavenumber space.
		\item \citeauthor{Sirgue} \cite{Sirgue} proposed a selection strategy of the temporal
			frequencies $f_0$ to maximize the coverage of the cloud.
		\item We propose instead to optimize the wavenumber content via the
			positioningof the sources and receivers. 
	\end{itemize}
\end{frame}
\section{Study of the wavenumber cloud}

% Angular expression
\begin{frame}{Expression of the wavenumbers by angles}

	\begin{columns}
		\begin{column}{0.45\textwidth}
			\begin{figure}
		\vspace{- 0.5 cm}
		\hspace{- 0.5 cm}
		\input{images/figure.tex}
		\vspace{- 0.5 cm}
		\caption{Scheme of the diffraction}
\end{figure}
		\end{column}
		\begin{column}{0.45\textwidth}
 \pause
 \begin{itemize}
			\item[--] $x$: positon of the target point.
			\item[--] $s$: position of the source.
			\item[--] $r$: position of the receiver.
			\item[--] $c_0$: velocity of the medium.
			\item[--] $f_0$: mean frequency.
		\end{itemize}
		\pause
		Alternative fornula to \eqref{eq:ps+pr}, using angles:

		\begin{equation*}
				k(s, r)  = k_0 \cos \theta \left( \sin( \phi  ), \cos( \phi ) \right) \,
				,
		\end{equation*}
\pause
 where
		\begin{equation*}
				\phi  = \frac{\phi_s + \phi_r}{2}, \qquad \theta  = \pm \frac{\phi_s -
				\phi_r}{2} \, .
		\end{equation*}
		\end{column}

	\end{columns}

\end{frame}

% Size of the cloud
\begin{frame}{Size of the covered area}
	\begin{figure}[h]
		\centering
		\includegraphics[width = \textwidth]{circular_arcs}
		\caption{Circular arcs in a wavenumber cloud}
		\label{fig:circular_arcs}
	\end{figure}
\end{frame}


% envelope
\begin{frame}{Envelope of the cloud}

The domain $\W$ can be constructed by circular arcs,

\begin{equation*}
	\W = \bigcup_{\phi_s = - \phi_{max}}^{\phi_{max}} \text{Arc}_{\phi_s} \, ,
\end{equation*}

where $\text{Arc}_{\phi_s}$ denotes the circular arc of radius $r = k_0$ and
center $c = \left( k_0 \sin \phi_s, -  k_0 \cos \phi_s \right)$, generated by
angles $b = \phi_r - \frac{\pi}{2}$ spanning $[- \phi_{max} - \frac{\pi}{2}, \phi_{max} - \frac{\pi}{2} ]$.

\pause

	\begin{figure}[h]
		\centering
		\includegraphics[width = 0.5\textwidth]{envelope}
		\caption{Enveloppe of the wavenumber cloud.}
		\label{fig:envelope}
	\end{figure}
\end{frame}

% Problem
\begin{frame}{Regularity}

		\begin{block}{Problem}
	\begin{mdframed}
			Given an acquisition range on the surface ($\phi_{max}$ is thus given) and
			a certain number of sources and receivers, how do we place them so to
			regularly fill in the space inside the envelope?
	\end{mdframed}
		\end{block}

	\pause

	The solution of this regularity problem can be viewed as an instance of stable
	\textbf{Centroidal Voronoi Tesselation}.
	
\end{frame}

\section{Centroidal Voronoi Tessellation}

% CVT
\begin{frame}{Voronoi cells}
	$\Omega \subset \mathbb{R}^N$ is a bounded domain and  $X = \left( x_i \right)_{i = 1,
	\ldots, n} \subset \Omega$. \pause A Voronoi cell of $x_i$ is the set $ \Omega_i = \left\{ x \in \Omega \quad | \quad \| x - x_i \| \leqslant \|x -
		x_j\|, \forall j \neq i \right\}$ . \pause The Voronoi cells $\left\{ \Omega_i \right\}_{i = 1, \ldots, n}$ form a
	Voronoi Tessellation of $\Omega$.
	
	\pause

\begin{figure}[h]
\centering
\subfigure[A Voronoi tessellation]{
	\includegraphics[width = 0.3\textwidth]{voronoi_diagram.png}
\label{fig:voronoi_diagram}
}
%\hspace{4mm}
\subfigure[A CVT]{
	\includegraphics[width = 0.3\textwidth]{cvt.png}
\label{fig:cvt}
}
\subfigure[A different CVT]{
	\includegraphics[width = 0.3\textwidth]{cvt1.png}
\label{fig:cvt}
}
\caption{CVT's of an octagon.}
\label{fig:voronoi}
\end{figure}

\end{frame}

% Lloyd's method
\begin{frame}{Geometric characterization}

\begin{mdframed}
	\begin{defi}[{\cite[Definition 1]{bruno-levy}}]
		The Voronoi Tessellation $\left\{ \Omega_i \right\}_{i = 1, \ldots, n}$ is a
		\textit{Centroidal Voronoi Tesselation}
		if for all  $i = 1, \ldots, n$, we have $x_i = c_i$; that is, each seed coincides with the
		centroid of its Voronoi cell.
		\label{def1}
	\end{defi}
\end{mdframed}

\pause	

Finding a CVT can be understood as the solving the
system of nonlinear equations of the form

\begin{equation*}
	x_i = c_i  \, , i = 1, \ldots, n \, , 
\end{equation*}

with $c_i$ the centroid of $\Omega_i$.

\pause

\textbf{Lloyd's method} is an iterative method that updates the seeds with the
centroids of their Voronoi cells.

\end{frame}

% moving points
\begin{frame}{Inadequacy of Lloyd's method}
	\begin{figure}[h]
\centering
\subfigure[A Voronoi tesselation of $\W$.]{
	\includegraphics[width = .75\textwidth]{no_lloyd1.png}
	\label{fig:no_lloyd1}
}

\subfigure[Result of applying an operation of Lloyd's method.]{
	\includegraphics[width = .75\textwidth]{no_lloyd2.png}
	\label{fig:no_lloyd2}
}
\caption{An attempt at applying the update of a seed by the centroid of its
Voronoi cell for the problem of regularity of the wavenumber cloud.}
\label{fig:no_lloyd}
\end{figure}
\end{frame}

% energy function
\begin{frame}{Variational characterization}
We define the energy function:

		\begin{equation*}
			F ( X ) = \sum_{i = 1}^N \int_{y \in \Omega_i} \| y - x_i \|^2 dy .
			\label{eq:F}
		\end{equation*}

		\pause 

		Denoting $m_i$ the area of $\Omega_i$, the gradient of the energy function
		is given by the components:

		\begin{equation*}
			\left( \nabla F \left(X\right) \right)_i = 2 m_i \left( x_i - c_i \right) \, .
		\end{equation*}

		\pause

\begin{mdframed}
	\begin{defi}{ {\cite[Definition 2]{bruno-levy}} }
		A \textbf{Centroidal Voronoi Tessellation} of a closed domain $\Omega$ with $n$ seed
		points $X =\left\{  x_i  \right\}_{i = 1, \ldots, n}$ is the Voronoi
		tessellation given by the seeds $X_0$ which is a critical point of the CVT
		energy function $F(X)$. Furthermore, a CVT is called a \textbf{stable CVT} if $X_0$ is
		a local minimizer of $F(X)$, and it is called an \textbf{optimal CVT} if $X_0$ is a
		global minimizer of $F(X)$.
	\end{defi}
\end{mdframed}
		
\end{frame}

% CVT as optimization problem
\begin{frame}{Newton-based methods for the CVT}
	\begin{itemize}
		\item Lloyd's method can
also be expressed as a gradient descent

\begin{equation*}
	X^{(n + 1)} = X^{(n)} - \frac{1}{2} M (X^{(n)} )^{- 1} \nabla F
	(X^{(n)}) \, .
\end{equation*}

\item \citeauthor{bruno-levy} found the following result

	\begin{mdframed}
		\begin{thm}
			The 2D and 3D CVT functions are $C^2$ in $\Gamma_C$ if $\Omega$ is convex and the density function $\rho(X)$ is $C^2$.
		\end{thm}
	\end{mdframed}

\item In light of this new result, the authors propose to use a Newton method of quadratic convergence for the CVT
in the 2D and 3D case, where $H$ denotes the Hessian of $F$,

\begin{equation*}
	X^{(n + 1)} = X^{(n)} - H( X^{(n)} )^{- 1} \nabla F (X^{(n)}) \, .
	\label{eq:cvt_newton}
\end{equation*}

	\end{itemize}

\end{frame}

% Our plan
\begin{frame}{Regularity as an optimization problem}
	\begin{itemize}
		\item We define a function that gets us the wavenumbers
\begin{equation*}
\begin{tabular}{ccl}
	K : \; $[ - \phi_{max}, \phi_{max}]^{N_s + N_r}$ & $\longrightarrow$ & $\left(
	\R^2 \right)^{N_s \times N_r}$\\
    $\left( S, R \right)$ & $\longmapsto$ & $\left( k(s_i, r_j) \right)_{\substack{j=1, \cdots , n \\ i=1, \cdots , m}}$
\end{tabular}\, .
\end{equation*}
 \pause

\item We define a new energy function for the problem of regularity 
\begin{equation*}
	F_{\phi} = F \circ K : [ - \phi_{max}, \phi_{max} ]^{N_s + N_r}
	\xrightarrow{ K } \left( \R^2 \right)^{N_s \times N_r} \xrightarrow{ F
	}
	\R^+ \, .
\end{equation*}

 \pause
\item We obtain a minimization formulation of the problem:

\begin{equation*}
	\min_{(S, R)}  F_{\phi}(S, R) \, .
\end{equation*}

 \pause
\item We apply a gradient descent method to solve the problem, using the chain
	rule to get a formula for the gradient of $F_{\phi}$:

\begin{equation*} 
	\nabla F_{\phi} (x) = J_K(x)^T \cdot \nabla F\left(  K(x) \right) \, .
\end{equation*}

	\end{itemize}
\end{frame}

\section{Results}

% classic
\begin{frame}{Classic layout and its wavenumber cloud}
\begin{figure}[h]
	\centering
	\includegraphics[width = \textwidth]{initialization_merged.png}
	\caption{An intuitive acquisition layout and the resulting wavenumber that
		gets us $F_{\phi} = 5.4 \times 10^{- 3}$.}
\end{figure}
\end{frame}

% first result
\begin{frame}{A result of the optimization}
\begin{figure}[h]
	\centering
	\includegraphics[width = \textwidth]{first_result_merged.png}
	\caption{Optimized layout and wavenumber cloud, with $F_{\phi} = 3.9 \times 10^{-3}$.}
\end{figure}

\end{frame}

% classic voronoi
\begin{frame}{Voronoi tessellation from the classic layout}
\begin{figure}[h]
	\centering
	\includegraphics[width = \textwidth]{initialization_voronoi_merged.png}
	\caption{The Voronoi tessellation of the classic wavenumber cloud.}
\end{figure}
\end{frame}

% first result voronoi
\begin{frame}{Voronoi tesselation from the optimization result}
\begin{figure}[h]
	\centering
	\includegraphics[width = \textwidth]{first_result_voronoi_merged.png}
	\caption{The Voronoi tessellation of the optimizated wavenumber cloud.}
\end{figure}
\end{frame}

% CVT
\begin{frame}{Lower bound of $F_{\phi}$ from a stable CVT of $\W$.}
	\begin{figure}[h]
\centering
\subfigure[A stable CVT of $\W$ obtained via BFGS with $F = 2.76 \times 10^{-
3}$.]{
	\includegraphics[width = .9\textwidth]{cvt_W.png}
	\label{fig:cvt_W}
}
\subfigure[The Voronoi tessellation of $\W$ from the previous optimal
wavenumbers, $F = 3.9 \times 10^{- 3}$.]{
	\includegraphics[width = .9\textwidth]{first_result_cvt.png}
	\label{fig:vt_W}
}
\end{figure}
\end{frame}

% presentation initializations
\begin{frame}{Different initializations}
	\begin{enumerate}
	\item A staggered\footnote{No source and receiver share the same location.} regular positioning of the sources and receivers.
	\item Random positions of the $N_s$ sources and $N_r$ receivers on the surface
		generated via a continuous random uniform distribution on $[0, 10 000]$.
	\item An acquisition layout corresponding to angles $\phi_s$ and $\phi_r$
		forming a regular discretization of $[ - \phi_{max}, \phi_{max} ]$ shifted as to avoid duplicate wavenumebrs.
	\item An acquisition layout corresponding to $N_s + N_r$ uniformely generated
		angles $\phi_s$ and $\phi_r$ on $[ - \phi_{max}, \phi_{max} ]$.
\end{enumerate}
\end{frame}

\begin{frame}{Overall results}
For random configurations we took the average over 10 runs.
	\begin{table}[h]
		\resizebox{\textwidth}{!}{%
	\begin{tabular}{c|c|c|c|c|}
		\cline{2-5}
		\multicolumn{1}{c|}{} & - 1 -  & - 2 - & - 3 - & - 4 - \\
		\multicolumn{1}{c|}{} & Regular positions &  Random positions & Regular
		angle & Random angles \\
				\hline
				\multicolumn{1}{|c|}{Initial value of $F_{\phi}$} & $5.4 \times 10^{-
				3}$ & \cellcolor{violet}<4>{$ 1.43 \times 10^{-2}$} & $ 4.5 \times 10^{-3}$ & \cellcolor{violet}<4>{$
					1.35 \times 10^{-2}$}\\
		\hline
		\multicolumn{1}{|c|}{Optimal value of $F_{\phi}$} & \cellcolor{violet}<2>{$3.9 \times
			10^{- 3}$} &
			\cellcolor{violet}<3>{$3.7 \times 10^{- 3}$} & \cellcolor{violet}<2>{$ 3.9
				\times 10^{-3}$} &
				\cellcolor{violet}<3>{$3.62 \times 10^{- 3}$}\\
		\hline
		\multicolumn{1}{|c|}{Number of iterations} & $55$ &
		\cellcolor{violet}<5>{$87$} & $ 64
		$ & \cellcolor{violet}<5>{$ 98 $}\\
		\hline
		\multicolumn{1}{|c|}{Execution time: } & $96$ s &
		\cellcolor{violet}<6>{$140$ s} & $
			120 $ s & \cellcolor{violet}<6>{$145$ s} \\
		\hline
	\end{tabular}}
	\caption{Comparison of the results of different initializations for the
	optimization program.}
		\label{tab:initializations_regular}
\end{table}
\end{frame}

% Best result
\begin{frame}{Best (initial configuration)}
	\begin{figure}[h!]
	\centering
	\includegraphics[width = \textwidth]{best_initial_merged.png}
	\caption{The initial configuration that yields the best output of the program,
		initial value $F_{\phi}
	= 1.61 \times 10^{- 2}$.}
	\label{fig:best_result}
\end{figure}
\end{frame}

% Best result
\begin{frame}{Best (result)}
	\begin{figure}[h!]
	\centering
	\includegraphics[width = \textwidth]{best_result_merged.png}
	\caption{The best result returned by BFGS from all the previous runs, with energy function $F_{\phi}
	= 3.33 \times 10^{- 3}$.}
	\label{fig:best_result}
\end{figure}
\end{frame}

% Best result
\begin{frame}{Best (Voronoi tessellation)}
	\begin{figure}[h!]
	\centering
	\includegraphics[width = \textwidth]{best_voronoi_merged.png}
	\caption{The Voronoi tesselation of the best result, with energy function $F	= 3.33 \times 10^{- 3}$.}
	\label{fig:best_result}
\end{figure}
\end{frame}

\section{Conclusion and perspectives}

% Conclusion
\begin{frame}{Conclusion}
	\begin{enumerate}
		\item We proposed a new approach for the optimization of the wavenumber
			coverage in the FWI. 
		\item The acquisition layouts resulting from the optimization are not
			intuitive.
		\item A more in depth exploration of the optimization method is required to
			extract a pattern from the resulting layouts.
		\item Test the results in a FWI run.
		\item Test the method on more complex models (non-homegeneous medium, 3D
			space, diffraction points not centered).
	\end{enumerate}
\end{frame}

\begin{frame}{References}
\bibliographystyle{plainnat}
\bibliography{biblio}

\end{frame}

\begin{frame}{The end}
    \centering
    Thank you for your attention.
    
    Any question?
\end{frame}

\end{document}
