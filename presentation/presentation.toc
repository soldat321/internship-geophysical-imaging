\beamer@sectionintoc {1}{Full Waveform Inversion}{3}{0}{1}
\beamer@sectionintoc {2}{Study of the wavenumber cloud}{35}{0}{2}
\beamer@sectionintoc {3}{Centroidal Voronoi Tessellation}{45}{0}{3}
\beamer@sectionintoc {4}{Results}{62}{0}{4}
\beamer@sectionintoc {5}{Conclusion and perspectives}{78}{0}{5}
